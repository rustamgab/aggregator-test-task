using Refit;
using RoutesAggregator.ServiceLayer.Models;

namespace RoutesAggregator.ServiceLayer.Integration.Clients;

public interface IProviderTwoClient
{
    [Get("/api/v1/ping")]
    Task<HttpResponseMessage> IsAvailableAsync(CancellationToken cancellationToken);
    
    [Post("/api/v1/search")]
    Task<ApiResponse<ProviderTwoSearchResponse>> SearchAsync(ProviderTwoSearchRequest request, CancellationToken cancellationToken);
}