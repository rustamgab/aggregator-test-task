using Refit;
using RoutesAggregator.ServiceLayer.Models;

namespace RoutesAggregator.ServiceLayer.Integration.Clients;

public interface IProviderOneClient
{
    [Get("/api/v1/ping")]
    Task<HttpResponseMessage> IsAvailableAsync(CancellationToken cancellationToken);
    
    [Post("/api/v1/search")]
    Task<ApiResponse<ProviderOneSearchResponse>> SearchAsync(ProviderOneSearchRequest request, CancellationToken cancellationToken);
}