using RoutesAggregator.ServiceLayer.Integration.Clients;
using RoutesAggregator.ServiceLayer.Mappers;
using RoutesAggregator.ServiceLayer.Models;
using RoutesAggregator.ServiceLayer.Services;

namespace RoutesAggregator.ServiceLayer.Integration.ProviderAdapters;

public class ProviderOneService : IProviderAdapter
{
    private readonly IProviderOneClient _providerOneClient;

    public ProviderOneService(IProviderOneClient providerOneClient)
    {
        _providerOneClient = providerOneClient;
    }

    public async Task<bool> IsAvailableAsync(CancellationToken cancellationToken)
    {
        var response = await _providerOneClient.IsAvailableAsync(cancellationToken);
        return response.IsSuccessStatusCode;
    }

    public async Task<IEnumerable<Route>> SearchAsync(SearchRequest request, CancellationToken cancellationToken)
    {
        var providerRequest = RoutesMapper.MapProviderOneSearchRequest(request);
        
        var response = await _providerOneClient.SearchAsync(providerRequest, cancellationToken);
        
        if (!response.IsSuccessStatusCode || response.Content is null)
            return Array.Empty<Route>();

        var routes = FilterByUnfilteredFields(request, response.Content.Routes);
        
        return routes.Select(RoutesMapper.MapProviderOneRoute);
    }

    /// <summary>
    /// Filter by fields that are impossible to use in call to provider
    /// </summary>
    private IEnumerable<ProviderOneRoute> FilterByUnfilteredFields(SearchRequest request, ProviderOneRoute[] routes)
        => request.Filters?.MinTimeLimit != null ? routes.Where(r => r.TimeLimit >= request.Filters.MinTimeLimit) : routes;
}