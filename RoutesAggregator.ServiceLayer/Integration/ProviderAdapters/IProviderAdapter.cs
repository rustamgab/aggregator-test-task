using RoutesAggregator.ServiceLayer.Services;

namespace RoutesAggregator.ServiceLayer.Integration.ProviderAdapters;

public interface IProviderAdapter
{
    Task<bool> IsAvailableAsync(CancellationToken cancellationToken);
    Task<IEnumerable<Route>> SearchAsync(SearchRequest request, CancellationToken cancellationToken);
}