using RoutesAggregator.ServiceLayer.Integration.Clients;
using RoutesAggregator.ServiceLayer.Mappers;
using RoutesAggregator.ServiceLayer.Models;
using RoutesAggregator.ServiceLayer.Services;

namespace RoutesAggregator.ServiceLayer.Integration.ProviderAdapters;

public class ProviderTwoService : IProviderAdapter
{
    private readonly IProviderTwoClient _providerTwoClient;

    public ProviderTwoService(IProviderTwoClient providerTwoClient)
    {
        _providerTwoClient = providerTwoClient;
    }

    public async Task<bool> IsAvailableAsync(CancellationToken cancellationToken)
    {
        var response = await _providerTwoClient.IsAvailableAsync(cancellationToken);
        return response.IsSuccessStatusCode;
    }

    public async Task<IEnumerable<Route>> SearchAsync(SearchRequest request, CancellationToken cancellationToken)
    {
        var providerRequest = RoutesMapper.MapProviderTwoSearchRequest(request);
        
        var response = await _providerTwoClient.SearchAsync(providerRequest, cancellationToken);
        
        if (!response.IsSuccessStatusCode || response.Content is null)
            return Array.Empty<Route>();

        var routes = FilterByUnfilteredFields(request, response.Content.Routes);

        return response.Content.Routes.Select(RoutesMapper.MapProviderTwoRoute);
    }

    /// <summary>
    /// Filter by fields that are impossible to use in call to provider
    /// </summary>
    private IEnumerable<ProviderTwoRoute> FilterByUnfilteredFields(SearchRequest request, ProviderTwoRoute[] routes)
    {
        IEnumerable<ProviderTwoRoute> filteredRoutes = routes;
        
        if (request.Filters?.DestinationDateTime != null)
        {
            // На самом деле тут вопрос, как именно мы смотрим на дату. Но до таких деталей не стал погружаться, поскольку это лишь тестовое задание.
            // Нужно ли учесть время, или только на дату смотрим. Может надо получить все маршруты с датой меньше переданной.
            // Так как логика поставщиков от меня скрыта, то подглядеть и предположить не могу.
            filteredRoutes = filteredRoutes.Where(r => r.Arrival.Date.Date == request.Filters.DestinationDateTime.Value.Date);
        }

        if (request.Filters?.MaxPrice != null)
        {
            filteredRoutes = filteredRoutes.Where(r => r.Price <= request.Filters.MaxPrice);
        }

        return filteredRoutes;
    }
}