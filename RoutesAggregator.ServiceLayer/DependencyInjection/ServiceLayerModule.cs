using Microsoft.Extensions.DependencyInjection;
using RoutesAggregator.ServiceLayer.Integration.Clients;
using RoutesAggregator.ServiceLayer.Services;
using Refit;
using RoutesAggregator.ServiceLayer.Integration.ProviderAdapters;

namespace RoutesAggregator.ServiceLayer.DependencyInjection;

public static class ServiceLayerModule
{
    public static IServiceCollection AddServices(this IServiceCollection services)
    {
        services.AddScoped<ISearchService, SearchService>();
        services.AddScoped<IProviderAdapter, ProviderOneService>();
        services.AddScoped<IProviderAdapter, ProviderTwoService>();
        services.AddRefitClient<IProviderOneClient>().ConfigureHttpClient(cfg => cfg.BaseAddress = new Uri("https://7072b406-1ff0-433a-a9db-487b74fd51b4.mock.pstmn.io/provider-one"));
        services.AddRefitClient<IProviderTwoClient>().ConfigureHttpClient(cfg => cfg.BaseAddress = new Uri("https://7072b406-1ff0-433a-a9db-487b74fd51b4.mock.pstmn.io/provider-two"));
        return services;
    }
}