using RoutesAggregator.ServiceLayer.Models;
using RoutesAggregator.ServiceLayer.Services;

namespace RoutesAggregator.ServiceLayer.Mappers;

public static class RoutesMapper
{
    public static ProviderOneSearchRequest MapProviderOneSearchRequest(SearchRequest request) 
        => new ProviderOneSearchRequest
            {
                From = request.Origin,
                To = request.Destination,
                DateFrom = request.OriginDateTime,
                DateTo = request.Filters?.DestinationDateTime,
                MaxPrice = request.Filters?.MaxPrice
            };
    
    public static Route MapProviderOneRoute(ProviderOneRoute route)
        => new Route
            {
                Id = Guid.NewGuid(),
                Origin = route.From,
                Destination = route.To,
                OriginDateTime = route.DateFrom,
                DestinationDateTime = route.DateTo,
                Price = route.Price,
                TimeLimit = route.TimeLimit
            };
    
    public static ProviderTwoSearchRequest MapProviderTwoSearchRequest(SearchRequest request)
        => new ProviderTwoSearchRequest
            {
                Departure = request.Origin,
                Arrival = request.Destination,
                DepartureDate = request.OriginDateTime,
                MinTimeLimit = request.Filters?.MinTimeLimit
            };
    
    public static Route MapProviderTwoRoute(ProviderTwoRoute route)
        => new Route
            {
                Id = Guid.NewGuid(),
                Origin = route.Departure.Point,
                Destination = route.Arrival.Point,
                OriginDateTime = route.Departure.Date,
                DestinationDateTime = route.Arrival.Date,
                Price = route.Price,
                TimeLimit = route.TimeLimit
            };
}