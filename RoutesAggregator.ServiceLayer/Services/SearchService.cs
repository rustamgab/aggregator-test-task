using System.Runtime.InteropServices.JavaScript;
using Microsoft.Extensions.Caching.Memory;
using RoutesAggregator.ServiceLayer.Constants;
using RoutesAggregator.ServiceLayer.Exceptions;
using RoutesAggregator.ServiceLayer.Integration.ProviderAdapters;
using RoutesAggregator.ServiceLayer.Models;

namespace RoutesAggregator.ServiceLayer.Services;

/// <summary>
/// Реалзиация сервиса поиска.
/// Я не трогал предоставленные файлы вообще. Они здесь в том виде, в котором были предоставлены.
/// </summary>
public class SearchService : ISearchService
{
    private readonly IEnumerable<IProviderAdapter> _providers;

    public SearchService(IEnumerable<IProviderAdapter> providers)
    {
        _providers = providers;
    }

    public async Task<SearchResponse> SearchAsync(SearchRequest request, CancellationToken cancellationToken)
    {
        /*
         * По поводу кэширования написал большой коммент в SomeRoutesCachingService.cs
         */
        
        var routes = await SearchInAllProvidersAsync(request, cancellationToken);

        if (!routes.Any())
            throw new NotFoundException(Errors.NoRoutesFound);

        var (minPrice, maxPrice, minMinutesRoute, maxMinutesRoute) = CalculatePricesAndDurations(routes);

        return new SearchResponse
        {
            Routes = routes.ToArray(),
            MinPrice = minPrice,
            MaxPrice = maxPrice,
            MinMinutesRoute = minMinutesRoute,
            MaxMinutesRoute = maxMinutesRoute
        };
    }

    public async Task<bool> IsAvailableAsync(CancellationToken cancellationToken)
    {
        SemaphoreSlim semaphore = new(2); // Limit concurrent http requests
        
        var isAvailableTasks = _providers.Select(async provider =>
        {
            await semaphore.WaitAsync(cancellationToken);
            try
            {
                return await provider.IsAvailableAsync(cancellationToken);
            }
            finally
            {
                semaphore.Release();
            }
        }); 
        
        var responses = await Task.WhenAll(isAvailableTasks);

        // Хотя бы один поставщик жив, значит агрегатор тоже (стоило мне задать вопрос как именно определять "жив" ли агрегатор, но не стал, раз это тестовое, в реальном бизнес кейсе я обычно уточняю)
        return responses.Any(r => r == true);
    }
    
    /// <summary>
    /// Search in all registered providers
    /// </summary>
    private async Task<IEnumerable<Route>> SearchInAllProvidersAsync(SearchRequest request, CancellationToken cancellationToken)
    {
        SemaphoreSlim semaphore = new(2); // Limit concurrent http requests

        var searchTasks = _providers.Select(async provider =>
        {
            await semaphore.WaitAsync(cancellationToken);
            try
            {
                return await provider.SearchAsync(request, cancellationToken);
            }
            finally
            {
                semaphore.Release();
            }
        });

        return (await Task.WhenAll(searchTasks)).SelectMany(routes => routes);
    }

    /// <summary>
    /// Get max and min values of price and route duration among routes
    /// P.S. Логику метода можно было бы написать через MaxBy, MinBy, но я решил оптимизровать код по производительности.
    /// </summary>
    private (decimal minPrice, decimal maxPrice, int minMinutesRoute, int maxMinutesRoute) CalculatePricesAndDurations(
        IEnumerable<Route> routes)
    {
        var firstRoute = routes.First();
        decimal minPrice = firstRoute.Price;
        decimal maxPrice = firstRoute.Price;
        int minMinutesRoute = GetRouteDurationMinutes(firstRoute);
        int maxMinutesRoute = GetRouteDurationMinutes(firstRoute);

        foreach (var route in routes)
        {
            var duration = GetRouteDurationMinutes(route);
            minMinutesRoute = duration < minMinutesRoute ? duration : minMinutesRoute;
            maxMinutesRoute = duration > maxMinutesRoute ? duration : maxMinutesRoute;

            minPrice = route.Price < minPrice ? route.Price : minPrice;
            maxPrice = route.Price > maxPrice ? route.Price : maxPrice;
        }

        return (minPrice, maxPrice, minMinutesRoute, maxMinutesRoute);
    }

    /// <summary>
    /// Get route duration in minutes. Minutes between date from and date to
    /// </summary>
    private int GetRouteDurationMinutes(Route route)
        => (int)(route.DestinationDateTime.ToUniversalTime() - route.OriginDateTime.ToUniversalTime()).TotalMinutes;
}