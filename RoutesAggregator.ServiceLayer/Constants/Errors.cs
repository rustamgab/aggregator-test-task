namespace RoutesAggregator.ServiceLayer.Constants;

public class Errors
{
    public const string NoRoutesFound = "Не удалось найти маршруты по заданным фильтрам";
}