using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RoutesAggregator.ServiceLayer.Services;

namespace RoutesAggregator.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/routes")]
    public class RoutesController : ControllerBase
    {
        [HttpPost("search")]
        public async Task<ActionResult<SearchResponse>> SearchRoutesAsync(
            [FromBody] SearchRequest request,
            [FromServices] ISearchService searchService)
            => Ok(await searchService.SearchAsync(request, HttpContext.RequestAborted));

        [HttpGet("ping")]
        public async Task<ActionResult<bool>> IsAvailableAsync([FromServices] ISearchService searchService)
            => Ok(await searchService.IsAvailableAsync(HttpContext.RequestAborted));
    }
}
