using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using RoutesAggregator.ServiceLayer.Exceptions;

namespace RoutesAggregator.WebApi.Filters;

public class ExceptionsFilter : ExceptionFilterAttribute
{
    public override void OnException(ExceptionContext context)
    {
        var exception = context.Exception;

        context.Result = exception switch
        {
            NotFoundException => new JsonResult(new {Error = exception.Message}){StatusCode = 404},
            _ => new JsonResult(new {Error = exception.Message}){StatusCode = 500}
        };
        
        context.ExceptionHandled = true;
    }
}